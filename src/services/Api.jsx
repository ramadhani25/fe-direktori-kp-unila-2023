import axios from "axios";
import Config from "./Config";

const api = axios.create({
  baseURL: Config.BASE_URL,
});

export const getCategories = () => api.get("/kategori");
export const addCategory = (category) => api.post("/kategori/create", category);
export const updateCategory = (id, category) => api.put(`/kategori/edelkategori/${id}`, category);
export const deleteCategory = (id) => api.delete(`/kategori/edelkategori/${id}`);

export const getArticles = () => api.get("/artikel");
export const getArticlesbyId = (id) => api.get(`/artikel/${id}`);
export const addArticle = (article) => api.post("/artikel/create", article);
// export const addArticle = (article, config) => api.post("/artikel/create", article, config);
export const updateArticle = (id, article, config) => api.put(`/artikel/edelartikel/${id}`, article, config);
// export const deleteArticle = (id, config) => api.delete(`/artikel/edelartikel/${id}`, config);
export const deleteArticle = (id) => api.delete(`/artikel/edelartikel/${id}`);

export const getAllFile = () => api.get("/file");
export const getFilebyArtikel = (id) => api.get(`/file/artikel/${id}`);
export const getFilebyId = (id) => api.get(`/file/${id}`);
export const deleteFile = (id) => api.delete(`/file/edelfile/${id}`);
export const uploadFile = (file) => api.post("/file/upload", file);

export const loginUser = (user) => api.post("/user/login", user);
export const createUser = (user) => api.post("/user/create", user);

export const accessToken = (token) => api.post("/token/", token);
export const refreshToken = (token) => api.post("/token/refresh/", token);

export const getNewAccessToken = () => {
  const refreshToken = localStorage.getItem("refreshToken");
  if (!refreshToken) {
    return Promise.reject("No refresh token available.");
  }

  return api
    .post("/token/refresh/", { refresh_token: refreshToken })
    .then((response) => {
      const newAccessToken = response.data.access_token;
      localStorage.setItem("accessToken", newAccessToken);
      api.defaults.headers.common["Authorization"] = `Bearer ${newAccessToken}`;
      return newAccessToken;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
};
