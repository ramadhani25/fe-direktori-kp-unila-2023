import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Home from "./layout/Home";
import PageKategori from "./pages/PageKategori.jsx";
import Login from "./pages/Login.jsx";
import DetailArtikel from "./pages/DetailArtikel";
import Layout from "./layout/Layout";
import Dashboard from "./pages/Dashboard";
import Kegiatan from "./pages/Kegiatan";
import Kategori from "./pages/Kategori";
import PagesNotFound from "./pages/PagesNotFound";

import "./App.css";

import PrivateRoutes from "./utils/PrivateRoute";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/pagekategori" element={<PageKategori />} />
        <Route path="/login" element={<Login />} />
        <Route path="/artikel/:id" element={<DetailArtikel />} />

        {/* <Route element={<PrivateRoutes />}> */}
        <Route element={<Layout />}>
          <Route path="dashboard" element={<Dashboard />} />
          <Route path="artikel" element={<Kegiatan />} />
          <Route path="kategori" element={<Kategori />} />
        </Route>
        {/* </Route> */}

        <Route path="*" element={<PagesNotFound />} />
      </Routes>
    </Router>
  );
}

export default App;
