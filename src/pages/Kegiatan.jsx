import React, { useState, useEffect, useRef } from "react";
import { BsTrash, BsPlusSquare } from "react-icons/bs";
import { AiTwotoneEdit } from "react-icons/ai";
import { FaFileDownload } from "react-icons/fa";
import Modal from "../components/Modal";
import { getCategories, addArticle, getArticles, updateArticle, deleteArticle, getFilebyArtikel, getNewAccessToken, uploadFile } from "../services/Api";

const Kegiatan = () => {
  const [showModal, setShowModal] = useState(false);
  const resetFile = useRef(null);
  const [formData, setFormData] = useState([]);
  const [modalDelete, setModalDelete] = useState(false);
  const [listKategori, setListKategori] = useState([]);
  const [token, setToken] = useState("");
  const [articles, setArticles] = useState([]);
  const [createArtikel, setCreateArtikel] = useState({
    judul_artikel: "",
    kategori: "",
    deskripsi: "",
    banner: null,
    file: null,
    // tipe: "Document",
    // format: "PDF",
  });

  useEffect(() => {
    const fetchArticles = async () => {
      try {
        // const storedToken = localStorage.getItem("accessToken");
        // if (!storedToken) {
        //   const newToken = await getNewAccessToken();
        //   localStorage.setItem("accessToken", newToken);
        //   axios.defaults.headers.common["Authorization"] = `Bearer ${newToken}`;
        //   setToken(newToken);
        // } else {
        //   axios.defaults.headers.common["Authorization"] = `Bearer ${storedToken}`;
        // }

        const categoriesResponse = await getCategories();
        const listDataKategori = categoriesResponse.data.results;
        setListKategori(listDataKategori);
        // console.log("listDataKategori", listDataKategori);

        const articlesResponse = await getArticles();
        const articlesData = articlesResponse.data.data;
        setArticles(articlesData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchArticles();
  }, []);

  const createArticle = async () => {
    const articleData = {
      judul: createArtikel.judul_artikel,
      kategori: createArtikel.kategori,
      deskripsi: createArtikel.deskripsi,
      banner: createArtikel.banner,
    };

    await addArticle(articleData);
    // console.log(articleData);

    const articlesResponse = await getArticles();
    const articlesData = articlesResponse.data.data;
    setArticles(articlesData);
    setShowModal(false);

    // Langkah 2: Unggah File
    const formData = new FormData();
    formData.append("artikel", articlesData.id);
    formData.append("judul_artikel", articlesData.judul_artikel);
    formData.append("file", createArtikel.file[0]);
    formData.append("banner", createArtikel.banner[0]);
    console.log("formData", formData);
    await uploadFile(formData);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    // console.log(createArtikel);
    try {
      await createArticle();
      console.log("Artikel berhasil dibuat dan file diunggah.");
      setCreateArtikel({
        judul_artikel: "",
        kategori: "",
        deskripsi: "",
        banner: null,
        file: null,
      });
      resetFile.current.value = null;
    } catch (error) {
      console.error("Error creating artikel:", error);
    }
    // try {
    //   const storedToken = localStorage.getItem("accessToken");
    //   if (!storedToken) {
    //     const newToken = await getNewAccessToken();
    //     localStorage.setItem("accessToken", newToken);
    //     axios.defaults.headers.common["Authorization"] = `Bearer ${newToken}`;
    //     setToken(newToken);
    //   } else {
    //     axios.defaults.headers.common["Authorization"] = `Bearer ${storedToken}`;
    //   }

    //   const config = {
    //     headers: {
    //       Authorization: `Bearer ${storedToken}`,
    //       "Content-Type": "multipart/form-data",
    //     },
    //   };
    // console.log(createArtikel);
    // const formData = new FormData();
    // formData.append("judul", createArtikel.judul_artikel);
    // formData.append("kategori", listKategori.id);
    // formData.append("deskripsi", createArtikel.deskripsi);
    // formData.append("file", createArtikel.file[0]);
    // formData.append("tipe", createArtikel.tipe);
    // formData.append("format", createArtikel.format);

    // // const response = await addArticle(formData, config);
    // const response = await addArticle(formData);
    // const listArtikel = response.data.results;
    // setCreateArtikel(listArtikel);
    // } catch (error) {
    //   console.error("Error adding article:", error);
    // }
  };

  // const handleDeleteEntry = (index) => {
  //   const updatedFormData = formData.filter((_, i) => i !== index);
  //   setFormData(updatedFormData);
  // };

  const editArtikel = async (id, newData) => {
    try {
      // const config = {
      //   headers: {
      //     Authorization: `Bearer ${token}`,
      //     "Content-Type": "multipart/form-data",
      //   },
      // };

      const formData = new FormData();
      formData.append("judul", newData.judul);
      formData.append("kategori", newData.kategori);
      formData.append("deskripsi", newData.deskripsi);
      formData.append("file", newData.file);

      // const response = await updateArticle(id, formData, config);
      const response = await updateArticle(id, formData);
      console.log("Data Berhasil Diubah!", response.data);

      const articlesResponse = await getArticles();
      const articlesData = articlesResponse.data.data;
      setArticles(articlesData);
    } catch (error) {
      console.error("Error editing article:", error);
    }
  };

  const deteleArtikel = async (id) => {
    try {
      // const config = {
      //   headers: {
      //     Authorization: `Bearer ${token}`,
      //     "Content-Type": "multipart/form-data",
      //   },
      // };

      // const response = await deleteArticle(id, config);
      const response = await deleteArticle(id);
      console.log("Data Telah Dihapus!", response.data);

      const articlesResponse = await getArticles();
      const articlesData = articlesResponse.data.data;
      setArticles(articlesData);
    } catch (error) {
      console.error("Error deleting article:", error);
    }
  };

  const downloadArtikel = async (id) => {
    try {
      const response = await getFilebyArtikel(id);
      const article = response.data.results;
      const fileURL = article.file;
      window.open(fileURL, "_blank");
    } catch (error) {
      console.error("Error fetching article data:", error);
    }
  };

  const getTanggal = (createdAtString) => {
    const createdAtDate = new Date(createdAtString);
    const tanggal = createdAtDate.getDate();
    const bulan = new Intl.DateTimeFormat("id-ID", { month: "long" }).format(createdAtDate);
    const tahun = createdAtDate.getFullYear();
    return `${tanggal < 10 ? "0" + tanggal : tanggal} ${bulan} ${tahun}`;
  };

  return (
    <div className="bg-white min-h-screen min-w-full p-3 ">
      <div className="m-1 bg-[#EFEFEF] rounded-md min-h-full pb-4">
        <div className="bg-[#EFEFEF] flex flex-row items-center justify-between p-3 rounded-xl">
          <h1 className="font-bold text-2xl">List Artikel APJII Directory</h1>
          <div>
            <button
              type="button"
              onClick={() => setShowModal(true)}
              className="flex flex-row text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-3 py-2.5 mr-1 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800 mt-4"
            >
              <BsPlusSquare size={20} className="me-2" />
              Tambah
            </button>
          </div>
        </div>
        <div className="m-3">
          <table className="table-fixed w-full border border-x-black bg-white">
            <thead>
              <tr className="border border-x-black">
                <th className="w-2/12 border border-black px-2 py-2">Tanggal</th>
                <th className="w-1/6 border border-black px-2 py-2">Judul</th>
                <th className="w-2/6 border border-black px-2 py-2">Deskripsi</th>
                <th className="w-1/6 border border-black px-2 py-2 text-center">Tipe</th>
                <th className="w-1/6 border border-black px-2 py-2 text-center">Kategori</th>
                <th className="w-1/6 border border-black px-2 py-2">Aksi</th>
              </tr>
            </thead>
            <tbody>
              {articles.length === 0 ? (
                <tr>
                  <td className="border border-black px-3 py-10 text-center text-xl" colSpan={6}>
                    Data Kosong
                  </td>
                </tr>
              ) : (
                articles.map((article) => (
                  <tr key={article.id} className="border border-x-black">
                    <td className="w-1/6 border border-black px-2 py-2">{getTanggal(article?.created_at)}</td>
                    <td className="w-2/6 border border-black px-2 py-2 text-justify">{article?.judul_artikel}</td>
                    <td className="w-1/6 border border-black px-2 py-2 text-center">{article?.deskripsi}</td>
                    <td className="w-1/6 border border-black px-2 py-2 text-center">{article?.tipe || "Document"}</td>
                    <td className="w-1/6 border border-black px-2 py-2 text-center">{article?.kategori?.nama}</td>
                    <td className="w-1/6 border border-black px-2 py-2">
                      <div className="flex justify-center items-center gap-2">
                        <button
                          onClick={() => {
                            const editedArticle = {};
                            editArtikel(article.id, editedArticle);
                          }}
                          type="button"
                          className="text-green-600 hover:text-green-800 focus:ring-2 focus:ring-green-300"
                        >
                          <AiTwotoneEdit size={25} />
                        </button>
                        <button
                          onClick={() => {
                            setModalDelete(true);
                          }}
                          type="button"
                          className="text-red-600 hover:text-red-800 focus:ring-2 focus:ring-red-300"
                        >
                          <BsTrash size={25} />
                        </button>
                        <button
                          onClick={() => {
                            downloadArtikel(article.id);
                          }}
                          type="button"
                          className="text-red-600 hover:text-red-800 focus:ring-2 focus:ring-red-300"
                        >
                          <FaFileDownload size={25} />
                        </button>
                      </div>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
        </div>

        <div>
          <Modal showModal={showModal}>
            <form onSubmit={handleSubmit} className="w-[480px]">
              <div className="mb-4">
                <label htmlFor="fileFormat" className="block text-gray-700 font-bold mb-2">
                  Pilih Kategori:
                </label>
                <select
                  id="fileFormat"
                  onChange={(e) => {
                    const selectedValue = e.target.value;
                    const selectedCategoryId = selectedValue !== "" ? parseInt(selectedValue) : "";
                    setCreateArtikel({ ...createArtikel, kategori: selectedCategoryId });
                  }}
                  value={createArtikel.kategori}
                  className="border border-gray-400 rounded-md py-2 pl-3 pr-8 w-full"
                  required
                >
                  {listKategori.length > 0 ? (
                    <React.Fragment>
                      <option value="">Pilih kategori</option>
                      {listKategori.map((kategori) => (
                        <option key={kategori.id} value={kategori.id}>
                          {kategori.nama}
                        </option>
                      ))}
                    </React.Fragment>
                  ) : (
                    <option value="">Tidak ada kategori tersedia</option>
                  )}
                </select>
              </div>

              <div className="mb-4">
                <label htmlFor="documentType" className="block text-gray-700 font-bold mb-2">
                  Judul:
                </label>
                <input type="text" id="documentType" value={createArtikel.judul_artikel} onChange={(e) => setCreateArtikel({ ...createArtikel, judul_artikel: e.target.value })} className="border border-gray-400 px-3 py-2 w-full" required />
              </div>

              <div className="mb-4">
                <label htmlFor="description" className="block text-gray-700 font-bold mb-2">
                  Deskripsi Singkat:
                </label>
                <textarea id="description" value={createArtikel.deskripsi} onChange={(e) => setCreateArtikel({ ...createArtikel, deskripsi: e.target.value })} className="border border-gray-400  px-3 py-2 w-full" required />
              </div>
              {/* 
              <div className="mb-4">
                <label htmlFor="tipe" className="block text-gray-700 font-bold mb-2">
                  Tipe:
                </label>
                <input type="text" id="tipe" value={createArtikel.tipe} onChange={(e) => setCreateArtikel({ ...createArtikel, tipe: e.target.value })} className="border border-gray-400 px-3 py-2 w-full" required placeholder="Document" />
              </div>

              <div className="mb-4">
                <label htmlFor="format" className="block text-gray-700 font-bold mb-2">
                  Format
                </label>
                <input type="text" id="format" value={createArtikel.format} onChange={(e) => setCreateArtikel({ ...createArtikel, format: e.target.value })} className="border border-gray-400 px-3 py-2 w-full" required placeholder="PDF" />
              </div> */}

              <div className="mb-4">
                <label htmlFor="img" className="block text-gray-700 font-bold mb-2">
                  Upload Banner (Image only):
                </label>
                <input
                  ref={resetFile}
                  type="file"
                  id="img"
                  accept=".png, .jpg, .jpeg, .HEIC"
                  onChange={(e) => {
                    setCreateArtikel({ ...createArtikel, banner: e.target.files[0] });
                  }}
                  className="border border-gray-400 rounded-md py-2 ps-[30px] w-full"
                  required
                />
              </div>

              <div className="mb-4 ">
                <label htmlFor="file" className="block text-gray-700 font-bold mb-2">
                  Upload File (PDF only):
                </label>
                <input ref={resetFile} type="file" id="file" accept=".pdf" onChange={(e) => setCreateArtikel({ ...createArtikel, file: e.target.files[0] })} className="border border-gray-400 rounded-md py-2 ps-[30px] w-full" required />
              </div>

              <div className="flex justify-end gap-3">
                <div>
                  <button onClick={() => setShowModal(false)} type="button" className="bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-white text-sm px-5 py-2.5 me-4">
                    Close
                  </button>
                </div>
                <div>
                  <button type="submit" className="bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-white text-sm px-5 py-2.5">
                    Upload
                  </button>
                </div>
              </div>
            </form>
          </Modal>

          <Modal showModal={modalDelete}>
            <h1>Apakah anda yakin akan menghapus artikel ini?</h1>
            <div className="flex justify-end mt-3">
              <button onClick={() => setModalDelete(false)} type="button" className="bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-white text-sm px-5 py-2.5 me-3">
                Cancel
              </button>

              <button
                onClick={() => {
                  deteleArtikel(articles[0].id);
                  setModalDelete(false);
                }}
                type="button"
                className="bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-white text-sm px-5 py-2.5"
              >
                Hapus
              </button>
            </div>
          </Modal>
        </div>
      </div>
    </div>
  );
};

export default Kegiatan;
