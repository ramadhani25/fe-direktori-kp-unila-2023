import React, { useState, useEffect } from "react";
import { BsTrash, BsPlusSquare } from "react-icons/bs";
import { AiTwotoneEdit } from "react-icons/ai";
import { FaFileDownload } from "react-icons/fa";
import Modal from "../components/Modal";
import { getCategories, addCategory } from "../services/Api";

const Kategori = () => {
  const [showModal, setShowModal] = useState(false);
  const [formData, setFormData] = useState([]);
  const [modalDelete, setModalDelete] = useState(false);
  // const [listKategori, setListKategori] = useState([]);
  const [token, setToken] = useState("");

  const [kategori, setKategori] = useState([]);

  const [createKategori, setCreateKategori] = useState([]);

  const getTanggal = (craetedAt) => {
    const date = new Date(craetedAt);
    const tanggal = date.getDate();
    const bulan = date.getMonth();
    const tahun = date.getFullYear();
    return `${tanggal}/${bulan}/${tahun}`;
  };

  useEffect(() => {
    const storedToken = localStorage.getItem("accessToken");
    setToken(storedToken);

    getCategories()
      .then((res) => {
        const listDataKategori = res.data.results;
        console.log("listDataKategori", listDataKategori);
        setKategori(listDataKategori);
      })
      .catch((err) => console.err("error", err));
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();

    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "multipart/form-data",
      },
    };

    const formData = new FormData();
    formData.append("nama", createKategori.nama);

    addCategory(formData, config)
      .then((res) => {
        const listArtikel = res.data.results;
        setCreateKategori(listArtikel);
      })

      .catch((err) => console.error("error", err));
  };

  const handleDeleteEntry = (index) => {
    const updatedFormData = formData.filter((_, i) => i !== index);
    setFormData(updatedFormData);
  };
  return (
    <div className="bg-white min-h-screen min-w-full p-3 ">
      <div className="m-1 bg-[#EFEFEF] rounded-md min-h-full pb-4">
        <div className="bg-[#EFEFEF] flex flex-row items-center justify-between p-3 rounded-xl">
          <h1 className="font-bold text-2xl">List Kategori APJII Directory</h1>
          <div>
            <button
              type="button"
              onClick={() => setShowModal(true)}
              className="flex flex-row text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-3 py-2.5 mr-1 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800 mt-4"
            >
              <BsPlusSquare size={20} className="me-2" />
              Tambah
            </button>
          </div>
        </div>
        <div className="m-3">
          <table className="table-fixed w-full border border-x-black bg-white">
            <thead>
              <tr className="border border-x-black">
                <th className="w-1/6 border border-black px-2 py-2">Tanggal</th>
                <th className="w-1/4 border border-black px-2 py-2">Nama</th>
                <th className="w-1/4 border border-black px-2 py-2">Author</th>
                <th className="w-1/3 border border-black px-2 py-2">Aksi</th>
              </tr>
            </thead>
            <tbody>
              {kategori.length === 0 ? (
                <tr>
                  <td className="border border-black px-3 py-10 text-center text-xl" colSpan={6}>
                    Data Kosong
                  </td>
                </tr>
              ) : (
                kategori.map((category, index) => (
                  <tr key={index} className="border border-x-black">
                    <td className="w-1/6 border border-black px-2 py-2">{getTanggal(category.created_at)}</td>
                    <td className="w-2/6 border border-black px-2 py-2 text-justify">{category.nama}</td>
                    <td className="w-1/6 border border-black px-2 py-2 text-center">Admin</td>

                    <td className="w-1/6 border border-black px-2 py-2">
                      <div className="flex justify-center items-center gap-2">
                        <button type="button" className="text-green-600 hover:text-green-800 focus:ring-2 focus:ring-green-300">
                          <AiTwotoneEdit size={25} />
                        </button>
                        <button onClick={() => setModalDelete(true)} type="button" className="text-red-600 hover:text-red-800 focus:ring-2 focus:ring-red-300">
                          <BsTrash size={25} />
                        </button>
                      </div>
                    </td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
        </div>

        <div>
          <Modal showModal={showModal}>
            <form onSubmit={handleSubmit} className="w-[480px]">
              <div className="mb-4">
                <label htmlFor="documentType" className="block text-gray-700 font-bold mb-2">
                  Nama:
                </label>
                <input type="text" id="documentType" value={createKategori.nama} onChange={(e) => setCreateKategori({ ...createKategori, nama: e.target.value })} className="border border-gray-400 px-3 py-2 w-full" required />
              </div>

              <div className="flex justify-end gap-3">
                <div>
                  <button onClick={() => setShowModal(false)} type="button" className="bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-white text-sm px-5 py-2.5 me-4">
                    Close
                  </button>
                </div>
                <div>
                  <button type="submit" className="bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-white text-sm px-5 py-2.5">
                    Upload
                  </button>
                </div>
              </div>
            </form>
          </Modal>
          <Modal showModal={modalDelete}>
            <h1>Apakah anda yakin akan menghapus artikel ini?</h1>
            <div className="flex justify-end mt-3">
              <button onClick={() => setModalDelete(false)} type="button" className="bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-white text-sm px-5 py-2.5 me-3">
                Cancel
              </button>

              <button
                onClick={() => {
                  handleDeleteEntry(formData.length - 1);
                  setModalDelete(false);
                }}
                type="button"
                className="bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-white text-sm px-5 py-2.5"
              >
                Hapus
              </button>
            </div>
          </Modal>
        </div>
      </div>
    </div>
  );
};

export default Kategori;
